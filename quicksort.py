import random

# Quicksort: Used to sort an array of numbers

def quick_sort(arr, low, high):
    """
    Recursive quicksort, makes use of partition method

    arr List: name of the array to be sorted
    low int: starting index for sorting the array
    high int: last index for sorting the array
    Return int: total number of sorted items

    """
    count = 0
    if low < high:
        i, count = partition(arr, low, high)
        count += quick_sort(arr, low, i - 1)
        count += quick_sort(arr, i + 1, high)
    return count


def partition(arr, low, high):
    """
    Partitions and sorts a given section of the array

    arr List: name of the array to be partitioned
    low int: starting index for partitioning
    high int: last index for partitioning
    Return int, int: new starting position for quicksort, total number of items in partition

    """
    count = 0
    pivot = arr[high]
    i = low - 1
    for j in range(low, high):
        count += 1
        if arr[j] <= pivot:
            i += 1
            swap(arr, i, j)

    swap(arr, i + 1, high)
    return i + 1, count


def quick_sort_random(arr, low, high):
    """
    Recursive quicksort, makes use of randomized partition method

    arr List: name of the array to be sorted
    low int: starting index for sorting the array
    high int: last index for sorting the array
    Return int: total number of sorted items

    """
    count = 0
    if low < high:
        i, count = partition_random(arr, low, high)
        count += quick_sort_random(arr, low, i - 1)
        count += quick_sort_random(arr, i + 1, high)
    return count


def partition_random(arr, low, high):
    """
    Partitions and sorts a given section of the array *randomly*

    arr List: name of the array to be partitioned
    low int: starting index for partitioning
    high int: last index for partitioning
    Return int, int: new starting position for quicksort, total number of items in partition

    """
    count = 0
    pivot = random.randint(low, high)
    i = low - 1
    for j in range(low, high):
        count += 1
        if arr[j] <= pivot:
            i += 1
            swap(arr, i, j)

    swap(arr, i + 1, high)
    return i + 1, count


def swap(arr, i, j):
    """
    Swap two elements in an array.

    arr List: array containing the elements to be switched
    i int: index of first item to be switched
    j int: index of second item to be switched

    """
    temp = arr[i]
    arr[i] = arr[j]
    arr[j] = temp
