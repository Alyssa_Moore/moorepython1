## Overview

Quicksort reads in a text file of integers (one number 
per line), sorts them, and saves the sorted list of 
integers to a new file. There are 3 main files: main.py,
quicksort.py, and file_controller.py.

## main.py

main.py is very lean; literally all that it does is invoke
the file_controller.py once to read in a file, quicksort.py 
once to sort the numbers, and file_controller.py once more 
to write out the results to a text file.

## quicksort.py

quicksort.py includes these methods: quick_sort, partition, 
and swap. quick_sort is the recursive method, invoking 
partition along the way as it does. Swap is a helper method
for the partition method, it is used to swap two elements 
in an array.

## file_controller.py

file_controller.py includes these methods: read_file_to_array.py, 
and read_array_to_file. The former passes in the name of a 
text file, appends a new array line-by-line with the contents 
of the file, and returns the array. The former does the opposite: 
passes in an array and name of a file, writes each element 
in the array to a line in a new file, then saves the file 
with the word "-sorted" appended to the present working 
directory.

## How to run it

To invoke the program with the supplied example file
(money.txt), simply invoke "python main.py money"
from the command line while within the directory, 
and the program will save a sorted list to a text 
file in the same directory (money-sorted.txt). You 
can also test out the program with a list of numbers 
of your own; just make sure that:
1. Each number is an integer (no decimals);
2. There is only one number per line; and,
3. The *name* of the file cannot end in ".txt"; see example 
file provided.