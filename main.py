#!/usr/bin/env python
import sys
from file_controller import *
from quicksort import *

"""
This program reads numbers from a file to an array, sorts the array using quicksort, writes
the sorted array to a new file, and prints metadata for the user (# of comparisons, etc.)
"""


def print_info(arr, count, half_count, quarter_count):
    """
    Collect information about data processed (how many items,
    number of comparisons, etc.) and print it for the user.

    arr List: now-sorted array
    count int: number of comparisons in entire array
    half_count int: number of comparisons for half of the array
    quarter_count int: number of comparisons for a quarter of the array

    """
    items = len(arr)
    print("\nTotal number of items processed: " + str(items))
    print("Total number of comparisons: " + str(count))
    print("Number of comparisons for first half of list: " + str(half_count))
    print("Number of comparisons for first quarter of list: " + str(quarter_count))


if __name__ == '__main__':

    # Quicksort
    numbers1 = list(map(int, read_file_to_array(sys.argv[1])))
    numbers2 = numbers1.copy()
    numbers3 = numbers1.copy()
    comp_count = quick_sort(numbers1, 0, len(numbers1) - 1)
    comp_count_half = quick_sort(numbers2, 0, (len(numbers2) - 1)//2)
    comp_count_quarter = quick_sort(numbers3, 0, (len(numbers3) - 1)//4)
    write_array_to_file(numbers1, sys.argv[1])
    print("\nQuicksort data:")
    print_info(numbers1, comp_count, comp_count_half, comp_count_quarter)

    # Randomized quicksort (doesn't add the new sorted file because it was already created above)
    rand_numbers1 = list(map(int, read_file_to_array(sys.argv[1])))
    rand_numbers2 = rand_numbers1.copy()
    rand_numbers3 = rand_numbers1.copy()
    rand_comp_count = quick_sort_random(rand_numbers1, 0, len(rand_numbers1) - 1)
    rand_comp_count_half = quick_sort_random(rand_numbers2, 0, (len(rand_numbers2) - 1)//2)
    rand_comp_count_quarter = quick_sort_random(rand_numbers3, 0, (len(rand_numbers3) - 1)//4)
    print("\nRandomized quicksort data:")
    print_info(rand_numbers1, rand_comp_count, rand_comp_count_half, rand_comp_count_quarter)
