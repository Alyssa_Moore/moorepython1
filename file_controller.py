def read_file_to_array(arg):
    """
    Takes in a filename, reads contents of file to an array line-by-line

    arg str: name of file to be read in
    Return List: array of items read in from the file

    """
    unsorted_list = []
    with open(arg, 'r') as f:
        for line in f:
            unsorted_list.append(line)
    return unsorted_list


def write_array_to_file(arr, name):
    """
    Writes contents of passed array to a new file

    arr str: name of array to be written to a file
    name str: name of the file being written to

    """
    with open(name + "-sorted", "w") as f:
        for x in arr:
            f.write(str(x) + "\n")
